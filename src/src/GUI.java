package src;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * @author John
 *
 */
public class GUI extends Application {

		public static void main(String[] args){
			
			launch(args);
		}
		
		@Override
		public void start(Stage primaryStage) {
			
			Exceptions eXc= new Exceptions();
			
			//Sets the title of the program
	        primaryStage.setTitle("Converter");
	        
	        //Sets up a grid
	        GridPane grid = new GridPane();
	        grid.setAlignment(Pos.TOP_LEFT);
	        grid.setHgap(-100);
	        grid.setVgap(10);
	        grid.setPadding(new Insets(10, 10, 10, 10));
	        
	        //Sets main intro
	        Text scenetitle = new Text("Let's Convert Some Numbers!");
	        scenetitle.setFont(Font.font("Comic Sans MS", FontWeight.NORMAL, 20));
	        grid.add(scenetitle, 0, 0);
	        
	        //Decimal
	        Text decimal = new Text("Decimal             :");
	        decimal.setFont(Font.font("Comic Sans MS", FontWeight.NORMAL, 20));
	        grid.add(decimal, 0, 1);
	        
	        //The IEEE-754 single precision floating point standard uses an 8-bit exponent (with a bias of 127) and a 23-bit significand.
	        Text single = new Text("Single Precision :");
	        single.setFont(Font.font("Comic Sans MS", FontWeight.NORMAL, 20));
	        grid.add(single, 0, 2);
	        
	        //The IEEE-754 double precision standard uses an 11-bit exponent (with a bias of 1023) and a 52-bit significand.
	        Text dubs = new Text("Double Precision:");
	        dubs.setFont(Font.font("Comic Sans MS", FontWeight.NORMAL, 20));
	        grid.add(dubs, 0, 3);
	        
	        //Hexadecimal
	        Text hex = new Text("Hexidecimal      :");
	        hex.setFont(Font.font("Comic Sans MS", FontWeight.NORMAL, 20));
	        grid.add(hex, 0, 4);
	       
	        //Input
	        TextField textField1= new TextField();
	        textField1.setMinSize(300, 10);
	        grid.add(textField1, 1, 1);
	        
	        //Input
	        TextField textField2= new TextField();
	        grid.add(textField2, 1, 2);
	        
	        //Input
	        TextField textField3= new TextField();
	        grid.add(textField3, 1, 3);
	      
	        //Input
	        TextField textField4= new TextField();
	        grid.add(textField4, 1, 4);
	        
	        //Button
	        Button button= new Button("Convert!");
	        grid.add(button, 5, 5);
	        
	        //Clear
	        Button clear= new Button("Clear");
	        grid.add(clear, 1, 5);

	        //Listener for the second search button
	        button.setOnAction(new EventHandler<ActionEvent>(){
	        	
	        	@Override
	        	public void handle(ActionEvent e){
	        		
	        		Methods m= new Methods();
	        		
	        		if(!textField1.getText().trim().isEmpty()){
	        			
	        			textField2.setText(m.dec2sp(Double.parseDouble(textField1.getText())));
	        			textField3.setText(m.dec2dp(Double.parseDouble(textField1.getText())));
	        			textField4.setText(m.dec2hex(Double.parseDouble(textField1.getText())));
	        		}else if(!textField2.getText().trim().isEmpty()){
	        			
	        			textField1.setText(Double.toString(m.sp2dec(textField2.getText())));
	        			textField3.setText((m.sp2dp(textField2.getText())));
	        			textField4.setText(m.dec2hex((m.sp2dec(textField2.getText()))));
	        		}else if(!textField3.getText().trim().isEmpty()){
	        			
	        			textField1.setText(Double.toString(m.dp2dec(textField3.getText())));
	        			textField2.setText(m.dec2sp(m.dp2dec(textField3.getText())));
	        			textField4.setText(m.dec2hex(m.dp2dec(textField3.getText())));
	        		}else if(!textField4.getText().trim().isEmpty()){
	        			
	        			textField1.setText(Double.toString(m.hex2dec(textField4.getText())));
	        			textField2.setText(m.dec2sp(m.hex2dec(textField4.getText())));
	        			textField3.setText(m.dec2dp(m.hex2dec(textField4.getText())));
	        		}else{
	        			
	        			eXc.reportLine(eXc.getLineNumber());
	        		}
	        	}
	        });
	        
	        //Listener for the second search button
	        clear.setOnAction(new EventHandler<ActionEvent>(){
	        	
	        	@Override
	        	public void handle(ActionEvent e){
	        		
	        		textField1.clear();
	        		textField2.clear();
	        		textField3.clear();
	        		textField4.clear();
	        	}
	        });
	        //Sets the stage and makes it visible
	        primaryStage.setScene(new Scene(grid, 600, 250));
	        primaryStage.show();
	    }
}
