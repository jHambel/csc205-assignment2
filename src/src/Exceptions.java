package src;

public class Exceptions {

	public void reportLine(int line){
		
		System.err.println("Something went wrong @ line: " + line);
	}
	
	public int getLineNumber() {
	    
		return Thread.currentThread().getStackTrace()[2].getLineNumber();
	}
	
}
