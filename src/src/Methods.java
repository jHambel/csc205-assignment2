package src;

public class Methods {

	int bitsVal;
	String bitsString;
	Exceptions eXc= new Exceptions();
	
	//Bias of 127
	public String dec2sp(double temp){

        float f = (float)temp;
        bitsVal = Float.floatToIntBits(f);
        bitsString = Integer.toBinaryString(bitsVal);

        return bitsString;
	}

	//Bias of 1023
	public String dec2dp(double temp){

		if(temp<0){

			//Remove the negative sign
			String t= Double.toString(temp);
			t=t.substring(1);
			double t2= Double.parseDouble(t);
			bitsString = Long.toString(Double.doubleToLongBits(t2), 2);
			
			//Check to see length
			if(bitsString.length()== 64){
				
				//This test case shouldnt happen
				eXc.reportLine(eXc.getLineNumber());
			}else if(bitsString.length()== 63){

				bitsString= "1" + bitsString;
			}else{
				
				eXc.reportLine(eXc.getLineNumber());
			}
			//If length matches, change first index

			//If length does not match, add to front
		}else{

			bitsString = Long.toString(Double.doubleToLongBits(temp), 2);
		}
	
	//Pad with 0
		//Check to see length
		if(bitsString.length()== 64){
			
			//This test case shouldnt happen
			eXc.reportLine(eXc.getLineNumber());
		}else if(bitsString.length()== 63){

			bitsString= "0" + bitsString;
		}else{
			
			eXc.reportLine(eXc.getLineNumber());
		}
		return bitsString;
	}

	public String sp2dp(String temp){

		double t= sp2dec(temp);
		return dec2dp(t);
	}

	public double sp2dec(String temp){

          float deciFinal;
          deciFinal = Float.intBitsToFloat((int) Long.parseLong(temp, 2));
          return deciFinal;
	}

	public double dp2dec(String temp){
		
		if(temp.substring(0,1).equals("1")){
			
			temp= "0" + temp.substring(1);
			bitsString = Double.toString(Double.longBitsToDouble(Long.parseLong(temp, 2)));
	        double tt= 0- Double.parseDouble(bitsString);
	        return tt;
		}else{
			
			bitsString = Double.toString(Double.longBitsToDouble(Long.parseLong(temp, 2))); 
	        return Double.parseDouble(bitsString);
		}
	}

	public double hex2dp(String temp){

		long longBits = Long.valueOf(temp,16).longValue();
		double doubleValue = Double.longBitsToDouble(longBits);
		return doubleValue;
	}

	public String dec2hex(double temp){

		return Integer.toHexString((int) temp);
	}

	public double hex2dec(String temp){

		return Integer.parseInt(temp.trim(), 16 );
	}
}
